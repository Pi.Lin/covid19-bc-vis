#!/bin/sh
python update_data.py
awk -i inplace '
    BEGIN { FS = OFS = "," } 
    { if (NR!=1 && index($1,"/") != 0) { split($1, date, /\//) ; 
                   $1 = sprintf("%02d-%02d-%02d",date[3],date[1],date[2]);
                 }
      print $0 
    }
' data/BCCDC_COVID19_Dashboard_Case_Details.csv
