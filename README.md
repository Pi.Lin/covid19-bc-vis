# covid19-bc-vis
COVID-19 in BC Data Visualization 

## Installation (for Debian or Ubuntu)

```
sudo apt-get install git python3-pip
sudo pip3 install pipenv
git clone https://gitlab.com/Pi.Lin/covid19-bc-vis
cd covid19-bc-vis
pipenv install
```

![Screenshot_from_2023-01-07_16-54-10](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/9bf138874ea3c735d3cb7ce977bd106f/Screenshot_from_2023-01-07_16-54-10.png)

## Usage

```
cd ~/covid19-bc-vis
./start
```

![Screenshot_from_2023-01-07_16-55-53](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/12c00ce2084250cc20508d11833301ec/Screenshot_from_2023-01-07_16-55-53.png)



## User Interface 

- for Desktop users

![Screenshot_from_2023-01-13_12-04-03](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/89cfaaa80fa32365cb57f55a85549395/Screenshot_from_2023-01-21_09-27-04.png)



## Visuals


![Screenshot_from_2023-01-10_18-21-49](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/d5c605abc6deef862f2095b11ea97da3/Screenshot_from_2023-01-20_07-44-04.png)

![Screenshot_from_2023-01-10_18-21-44](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/743017391f7b39e2f3b69f7fbdce053a/Screenshot_from_2023-01-10_18-21-44.png)

![Screenshot_from_2023-01-10_18-22-10](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/46898ca8dd2b288e244dbcb2f4c6b9cc/Screenshot_from_2023-01-10_18-22-10.png)

![Screenshot_from_2023-01-10_18-22-29](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/f0ec86af4ea32b1c5237a16ef5e15b9e/Screenshot_from_2023-01-10_18-22-29.png)

![Screenshot_from_2023-01-10_18-22-41](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/577bfa1e811db8cd39aa7378dec6d02f/Screenshot_from_2023-01-10_18-22-41.png)

![Screenshot_from_2023-01-10_18-22-59](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/df91d30ed02d1cd8f71b90c50ed8b4fc/Screenshot_from_2023-01-10_18-22-59.png)

![Screenshot_from_2023-01-10_18-23-20](https://gitlab.com/Pi.Lin/covid19-bc-vis/-/wikis/uploads/fdf7d53e7ecef9ad2eca7064a1688a7e/Screenshot_from_2023-01-20_07-53-25.png)


## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

- [Peter Lin](mailto:lin.pifeng@gmail.com)

## Deployed to PythonAnywhere 

https://linpf.pythonanywhere.com/
