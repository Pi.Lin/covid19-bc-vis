import requests

def scheduled_job():

    print('This job is run every weekday at 4:50')

    url = 'http://www.bccdc.ca/Health-Info-Site/Documents/BCCDC_COVID19_Dashboard_Case_Details.csv'
    r = requests.get(url)
    with open('data/BCCDC_COVID19_Dashboard_Case_Details.csv', 'wb') as f:
        f.write(r.content)
        print(len(r.content))

    url = 'http://www.bccdc.ca/Health-Info-Site/Documents/BCCDC_COVID19_Dashboard_Lab_Information.csv'
    r = requests.get(url)
    with open('data/BCCDC_COVID19_Dashboard_Lab_Information.csv', 'wb') as f:
        f.write(r.content)
        print(len(r.content))

    url = 'http://www.bccdc.ca/Health-Info-Site/Documents/BCCDC_COVID19_Regional_Summary_Data.csv'
    r = requests.get(url)
    with open('data/BCCDC_COVID19_Regional_Summary_Data.csv', 'wb') as f:
        f.write(r.content)
        print(len(r.content))

if __name__ == "__main__":
    scheduled_job()
